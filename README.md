# Fifa

Proyecto de aplicacion web para la fifa con el fin de insertar, consultar, modificar y eliminar equipos, jugadores y tecnicos que asistiran al siguiente mundial, creado usando [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.

## Para Empezar
Inicia clonando el proyecto usando el controlador de versiones de eleccion. los ejemplos siguientes se realizaran con git
ejecuta `git init` en la carpeta que desees almacenar el proyecto, luego clonalo utilizando `git clone (enlace ssh o link)`
ejecuta `ng serve` para iniciar el servidor de desarrollador. usando un explorador de internet navega hasta `http://localhost:4200/`. la aplicacion se conectara automaticamente a la base de datos y se podra usar de inmediato.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Pre Requisitos

Node js
Angular
navegador web o compilador
conexion a internet

## Test de unidad

Ejecuta `ng test` para realizar los test unitarios via [Karma](https://karma-runner.github.io).

## Test end-to-end

Ejecuta `ng e2e` Para ejecutar el test de extremo a extremo utilizando [Protractor](http://www.protractortest.org/).

## Construido Utilizando

[firebase](https://firebase.google.com/).
[AngularFire2](https://github.com/angular/angularfire2).
[bitbucket](https://bitbucket.org/).
[VisualStudioCode](https://code.visualstudio.com/).

##Autor
Juan Pablo Quiroga Baena
