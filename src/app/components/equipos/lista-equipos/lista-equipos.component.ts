import { Component, OnInit } from '@angular/core';



//service
import { EquipoService } from '../../../servicios/equipo.service'
import { JugadorService }from '../../../servicios/jugador.service'

//clase equipo
import { Equipo } from 'src/app/Variables/equipo';
import { isNgTemplate } from '@angular/compiler';


@Component({
  selector: 'app-lista-equipos',
  templateUrl: './lista-equipos.component.html',
  styleUrls: ['./lista-equipos.component.css']
})
export class ListaEquiposComponent implements OnInit {

  //array para la lista de equipos
    equipoList: Equipo[];

  //variable para cantidad de equipos
    cantidad: number;
  

  //constructor que llama a servicios de equipo
    constructor(
      private equipoServicio: EquipoService,
    ) { 
      
    }
  
    ngOnInit() {
      //generacion de la lista con for
      return this.equipoServicio.getEquipos()
        .snapshotChanges().subscribe(item => {
          this.equipoList = [];
          item.forEach(element => {
            let x = element.payload.toJSON();
            x["$keyEquipo"] = element.key;
            this.equipoList.push(x as Equipo);
          //cantidad de equipos inscritos
          this.cantidad=item.length;
        });
        });
        
        
      }
  
    onEdit (equipo: Equipo){

      //llama los datos del elemento seleccionado al formulario al hacer click en el boton
      this.equipoServicio.selectedEquipo = equipo;      
    
    }
    onDelete ($keyEquipo: string){

      //borra los datos del elemento seleccionado en la base de datos
      this.equipoServicio.deleteEquipo($keyEquipo);

    }
  }
  