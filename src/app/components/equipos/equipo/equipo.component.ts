import { Component, OnInit } from '@angular/core';


//service
import { EquipoService } from '../../../servicios/equipo.service';

//formularios
import { NgForm } from '@angular/forms';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

//Clase Equipo
import { Equipo } from 'src/app/Variables/equipo';
import { from } from 'rxjs';
import { stringify } from '@angular/compiler/src/util';

//variables
import { bandera } from '../../../Variables/equipo'

//firebase
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';



@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.component.html',
  styleUrls: ['./equipo.component.css']
})
export class EquipoComponent implements OnInit {

  //archivo Seleccionado de bandera
  selectedFiles: FileList;
  currentUpload: bandera;
  constructor(private equipoService: EquipoService, private storage:AngularFireStorage) { }

  ngOnInit() {
    this.equipoService.getEquipos();
    this.resetForm();
  }

  onSubmit(FormEquipo: NgForm){

    //llamada al metodo insertar con los datos del formulario

      if(FormEquipo.value.$keyEquipo == null){
        this.equipoService.insertEquipos(FormEquipo.value);

        //llamada al metodo de vaciado de datos

        this.resetForm(FormEquipo);
      }else{
      this.equipoService.updateEquipo(FormEquipo.value);
      }
             
  }

  //vaciado de datos para nueva insercion
  resetForm(FormEquipo?: NgForm){

    if(FormEquipo != null){}
    FormEquipo.reset();
    this.equipoService.selectedEquipo = new Equipo();
  }

  detectFilesEscudo($eventEscudo){
    const id = Math.random().toString(36).substring(2);
    const file = $eventEscudo.target.files[0];
    const filePath = `uploads/equipo${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
  }
  
  subirEscudo(eventEscudo){
    
    

   }

   subirBandera(eventBandera){
    const id = Math.random().toString(36).substring(2);
    const file = eventBandera.target.files[0];
    const filePath = `uploads/equipo${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
   }
  
}
