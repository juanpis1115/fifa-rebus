import { Component, OnInit } from '@angular/core';

//clase tecnico
import { Tecnico } from 'src/app/Variables/tecnico';

//servicio
import { TecnicoService } from 'src/app/servicios/tecnico.service';

@Component({
  selector: 'app-lista-tecnicos',
  templateUrl: './lista-tecnicos.component.html',
  styleUrls: ['./lista-tecnicos.component.css']
})
export class ListaTecnicosComponent implements OnInit {

    tecnicoList: Tecnico[];
  
    constructor(
      private tecnicoServicio: TecnicoService) { }
  
    ngOnInit() {
      return this.tecnicoServicio.getTecnico()
        .snapshotChanges().subscribe(item => {
          this.tecnicoList = [];
          item.forEach(element => {
            let x = element.payload.toJSON();
            x["$keyTecnico"] = element.key;
            this.tecnicoList.push(x as Tecnico);
          });
        });
    }
  
    onEdit (tecnico: Tecnico){

      this.tecnicoServicio.selectedTecnico = tecnico;      
    
    }
    onDelete ($keyTecnico: string){
      
      this.tecnicoServicio.deleteTecnico($keyTecnico);


    }
  }
  