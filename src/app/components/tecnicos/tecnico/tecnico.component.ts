import { Component, OnInit } from '@angular/core';


//service

import { TecnicoService } from '../../../servicios/tecnico.service';

//formularios
import { NgForm } from '@angular/forms';

//Clase Tecnico
import { Tecnico } from 'src/app/Variables/tecnico';

@Component({
  selector: 'app-tecnico',
  templateUrl: './tecnico.component.html',
  styleUrls: ['./tecnico.component.css']
})
export class TecnicoComponent implements OnInit {

  constructor(private tecnicoService: TecnicoService) { }


  ngOnInit() {
    this.tecnicoService.getTecnico();
    this.resetForm();
  }

  onSubmit(FormTecnico:NgForm){

    if (FormTecnico.value.$keyTecnico ==null){

      this.tecnicoService.InsertTecnico(FormTecnico.value);
      this.resetForm(FormTecnico);
    }else{
      this.tecnicoService.updateTecnico(FormTecnico.value);
    }
    

  }

  resetForm(FormTecnico?: NgForm){

    if(FormTecnico != null){
      FormTecnico.reset();
      this.tecnicoService.selectedTecnico = new Tecnico();
    }
  }

}

 