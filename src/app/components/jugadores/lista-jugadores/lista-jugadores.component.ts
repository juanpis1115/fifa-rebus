import { Component, OnInit } from '@angular/core';


//clase jugador
import { Jugador } from 'src/app/Variables/jugador';

//servicios jugador
import { JugadorService } from 'src/app/servicios/jugador.service';


@Component({
  selector: 'app-lista-jugadores',
  templateUrl: './lista-jugadores.component.html',
  styleUrls: ['./lista-jugadores.component.css']
})
export class ListaJugadoresComponent implements OnInit {

  jugadorList: Jugador[];
  cantidad: number;

  constructor(
    private jugadorServicio: JugadorService
  ) { }

  ngOnInit() {

    //obtencion de la lista de jugadores
    return this.jugadorServicio.getJugador()
      .snapshotChanges().subscribe(item => {
        this.jugadorList = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x["$keyJugador"] = element.key;
          this.jugadorList.push(x as Jugador);

          //cantidad de jugadores en la lista
          this.cantidad = item.length;
        });
      });
  }

  onEdit (jugador: Jugador){

    //trae los elementos seleccionados al formulario para su edicion
    this.jugadorServicio.selectedJugador = jugador;      
  
  }
  onDelete ($keyJugador: string){

    //borra los datos del elemento seleccionado en la base de datos
    this.jugadorServicio.deleteJugador($keyJugador);

  }
}
