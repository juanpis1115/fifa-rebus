import { Component, OnInit } from '@angular/core';


//service
import { JugadorService } from '../../../servicios/jugador.service';

//formularios
import { NgForm } from '@angular/forms';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

//Clase Equipo
import { Equipo } from '../../../Variables/equipo'

//Clase jugador
import { Jugador } from 'src/app/Variables/jugador';
import { from } from 'rxjs';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html',
  styleUrls: ['./jugador.component.css']
})
export class JugadorComponent implements OnInit {

  selectedEquipo: Equipo= new Equipo();

  constructor(private jugadorService: JugadorService) { }

  ngOnInit() {
   
    this.jugadorService.getJugador();
    this.resetForm();
    

  }

  onSubmit(FormJugador: NgForm){
    this.jugadorService.insertJugador(FormJugador.value);
    this.resetForm(FormJugador);
  }

  resetForm(FormJugador?: NgForm){

  if(FormJugador != null){}
    FormJugador.reset();
    this.jugadorService.selectedJugador = new Jugador();
  }

}
