import { Injectable } from '@angular/core';

//firebase
import {AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database'
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';

//variables
import { Equipo, bandera } from "../Variables/equipo"


@Injectable({
  providedIn: 'root'
})

export class EquipoService {

  //lista de inserciones en firebase
  ListaEquipos: AngularFireList<any>;

  //objeto para el equipo actualmente seleccionado
  selectedEquipo: Equipo= new Equipo();
  


  //constructor para los metodos firebase
  constructor(private firebase: AngularFireDatabase) {

   }

   //ruta de guardado de fotos
   private pathbase:string = '/fotosequipo';


   //Tarea de subida de fotos a firebase
   private uploadTask: firebase.storage.UploadTask;
   
   //metodos Equipos

   getEquipos(){
     return this.ListaEquipos = this.firebase.list("/equipos");
   }

   insertEquipos(equipo: Equipo){

    this.ListaEquipos.push({
      pais: equipo.pais,
      
    })
   }

   updateEquipo(equipo: Equipo){
     this.ListaEquipos.update(equipo.$keyEquipo, {
      pais: equipo.pais,
     });
   }

   deleteEquipo ($keyEquipo: string){
     this.ListaEquipos.remove($keyEquipo)
   }

}