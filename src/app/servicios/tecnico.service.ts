import { Injectable } from '@angular/core';

//firebase
import {AngularFireDatabase, AngularFireList } from 'angularfire2/database'


//variables 
import { Tecnico } from "../Variables/tecnico"

@Injectable({
  providedIn: 'root'
})
export class TecnicoService {

  ListaTecnicos: AngularFireList<any>;
  selectedTecnico: Tecnico= new Tecnico();

  constructor(private firebase: AngularFireDatabase) {

  }

  //metodos tecnicos

  getTecnico(){    
    return this.ListaTecnicos = this.firebase.list("/Tecnicos");
  }

  InsertTecnico(tecnico: Tecnico){
    
   this.ListaTecnicos.push({
    nombreTecnico: tecnico.nombreTecnico,
    apellidoTecnico: tecnico.apellidoTecnico,
    fechaNacimientoTecnico:tecnico.fechaNacimientoTecnico,
    nacionalidad:tecnico.nacionalidad,
    rol:tecnico.rol,
    paisTecnico:tecnico.paisTecnico
   })

  }

  updateTecnico(tecnico: Tecnico){
    this.ListaTecnicos.update(tecnico.$keytecnico, {
    nombreTecnico: tecnico.nombreTecnico,
    apellidoTecnico: tecnico.apellidoTecnico,
    fechaNacimientoTecnico:tecnico.fechaNacimientoTecnico,
    nacionalidad:tecnico.nacionalidad,
    rol:tecnico.rol,
    paisTecnico:tecnico.paisTecnico
    });
  }

  deleteTecnico ($keytecnico: string){
    this.ListaTecnicos.remove($keytecnico)
  }
  
}
