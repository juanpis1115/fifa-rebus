import { Injectable } from '@angular/core';

//firebase
import {AngularFireDatabase, AngularFireList } from 'angularfire2/database'


//variables
import{ Jugador } from"../Variables/jugador"
import { CompileStylesheetMetadata } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class JugadorService {

  ListaJugadores: AngularFireList<any>;
  selectedJugador: Jugador= new Jugador();
  
  constructor(private firebase: AngularFireDatabase) {
   }
  

   //metodos Jugadores

   getJugador(){    
    
    return this.ListaJugadores = this.firebase.list("/Jugadores");
  }

  insertJugador(jugador: Jugador){

   this.ListaJugadores.push({
    foto: jugador.foto,
    nombreJugador: jugador.nombreJugador,
    apellidoJugador: jugador.apellidoJugador,
    FechaNacimientoJugador: jugador.FechaNacimientoJugador,
    posicion: jugador.posicion,
    camiseta: jugador.camiseta,
    titular: jugador.titular,
    paisJugador:jugador.paisJugador
    });

  }

  updateJugador(jugador: Jugador){
    this.ListaJugadores.update(jugador.$keyjugador, {
    foto: jugador.foto,
    nombreJugador: jugador.nombreJugador,
    apellidoJugador: jugador.apellidoJugador,
    FechaNacimientoJugador: jugador.FechaNacimientoJugador,
    posicion: jugador.posicion,
    camiseta: jugador.camiseta,
    titular: jugador.titular,
    paisJugador:jugador.paisJugador
    });
  }

  deleteJugador ($keyjugador: string){
    this.ListaJugadores.remove($keyjugador)
  }




}
