//modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import {from} from 'rxjs'; 

//componentes
import { AppComponent } from './app.component';
import { EquiposComponent } from './components/equipos/equipos.component';
import { EquipoComponent } from './components/equipos/equipo/equipo.component';
import { ListaEquiposComponent } from './components/equipos/lista-equipos/lista-equipos.component';
import { JugadoresComponent } from './components/jugadores/jugadores.component';
import { JugadorComponent } from './components/jugadores/jugador/jugador.component';
import { ListaJugadoresComponent } from './components/jugadores/lista-jugadores/lista-jugadores.component';
import { TecnicosComponent } from './components/tecnicos/tecnicos.component';
import { TecnicoComponent } from './components/tecnicos/tecnico/tecnico.component';
import { ListaTecnicosComponent } from './components/tecnicos/lista-tecnicos/lista-tecnicos.component';



//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';

//entorno
import { environment } from '../environments/environment';


//Servicios
import { EquipoService } from './servicios/equipo.service';
import { JugadorService} from './servicios/jugador.service';
import { TecnicoService } from './servicios/tecnico.service';


@NgModule({
  declarations: [
    AppComponent,
    EquiposComponent,
    EquipoComponent,
    ListaEquiposComponent,
    JugadoresComponent,
    JugadorComponent,
    ListaJugadoresComponent,
    TecnicosComponent,
    TecnicoComponent,
    ListaTecnicosComponent
  ],
  imports: [
    BrowserModule,
    //imports de firebase
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireStorageModule,

    //import modulos
    FormsModule,

  ],
  providers: [
    //se importan servicios para usar en todos los componentes
    EquipoService,
    JugadorService,
    TecnicoService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
