export class Equipo {
    $keyEquipo: string;
    pais: string;
    cantidad: number;
}

export class bandera{
    $keyBandera: string;
    archivoBandera:File;
    nombreBandera:string;
    urlBandera:string;
    progresoBandera: number;

    constructor(archivo: File) {
        this.archivoBandera= this.archivoBandera;
    
    }
}

export class escudo{
    $keyEscudo: string;
    archivoEscudo:File;
    nombreEscudo:string;
    urlEscudo:string;
    progresoEscudo: number;

    constructor(archivoEscudo: File) {
        this.archivoEscudo= archivoEscudo;
    }
}