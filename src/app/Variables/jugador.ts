export class Jugador {
    $keyjugador: string;
    foto: string;
    nombreJugador: string;
    apellidoJugador: string;
    FechaNacimientoJugador: string;
    posicion: string;
    camiseta: number;
    titular: string;
    paisJugador: string;
}