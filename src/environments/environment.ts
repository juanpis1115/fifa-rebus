// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC1WMIML9bNU4_DjFroUXI5YYf1niqdLx0",
    authDomain: "fifa-12b2c.firebaseapp.com",
    databaseURL: "https://fifa-12b2c.firebaseio.com",
    projectId: "fifa-12b2c",
    storageBucket: "fifa-12b2c.appspot.com",
    messagingSenderId: "344962435120"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
